# Teste Programação Web - Bytecom #
Versão 1.0

## Objetivo ##
Este teste tem por intuito mensurar o nível de conhecimento técnico do candidato. Para tal, serão analisados os conhecimentos do candidato da seguinte forma: utilização da ferramenta de versionamento de código (Git), escolha da tecnologia apropriada para a resolução do problema proposto, e por fim, será avaliada a solução final proposta pelo candidato, a qual levará em conta a qualidade do código (lógica), documentação (primariamente comentários no código) e usabilidade.

## Escopo ##
O teste consiste em desenvolver o front-end de uma aplicação web que realiza o controle de pedidos de venda e clientes (de forma simplificada). 
	As tecnologias (linguagem, framework, IDE, etc) utilizadas para realizar o teste ficam a critério do candidato, podendo o mesmo selecionar de acordo com sua proficiência e/ou disponibilidade.

## Descrição da Aplicação ##

A aplicação deve ser desenvolvida com base nos dados providos pelo backend, cuja documentação pode ser encontrada em: [Link Docs](https://central.bytecom.inf.br:8085/teste/docs). Uma ideia geral do funcionamento da aplicação pode ser observado na Imagem 1. A aplicação é composta por três listas, sendo a lista de clientes, produtos e pedidos. A partir da lista de clientes será possível realizar o cadastro, edição e exclusão de clientes. Já a partir da lista de pedidos é possível realizar o cadastro de um novo pedido. Durante o cadastro do pedido deverão ser adicionados itens, o quais são criados com base em um produto, o qual deverá ser selecionado a partir da lista de produtos. 

Obs: Tenha em mente que estas são as funcionalidades básicas da aplicação, novas funcionalidades e comportamentos são bem vindos e serão levados em conta.

## Requisitos ##

### Requisitos Funcionais ###
* RF01: Exibir a lista de clientes já cadastrados;
* RF02: Realizar o cadastro de um novo cliente;
* RF03: Editar os dados de um cliente existente;
* RF04: Excluir um cliente existente;
* RF05: Visualizar lista de produtos;
* RF06: Visualizar lista de pedidos;
* RF07: Cadastrar novo pedido;

### Requisitos Não-Funcionais ###
* RNF01: A aplicação deverá apresentar um layout responsivo;
* RNF02: A aplicação deverá utilizar os dados providos pelo backend;
* RNF03: A aplicação deverá persistir novos dados utilizando o backend;
* RNF04:A aplicação deverá ser construída com base nas regras apresentadas no backend (obrigatoriedade de campos e máscaras);

### Regras de Negócio ###
* RN01: O pedido deve conter pelo menos 1 (um) item;
* RN02: Os itens do pedido são criados com base em um produto existente;
* RN03: A quantidade de um item de um pedido deve ser maior que 1 (um);
* RN04: Todos os pedidos devem conter um custo adicional fixo de  R$ 15,00 referente ao frete somando ao seu valor total;
* RN05: Após criado um pedido não pode mais ser editado;
* RN06: Todos os itens do pedido devem ser definidos (adicionados) durante a sua criação;
* RN07: O valor do produto não pode ser alterado;
